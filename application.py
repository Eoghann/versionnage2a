#! /usr/bin/env python3

from flask import Flask, render_template
from db import DB
app = Flask(__name__)
app.debug = True

todo = {
    "duchier" : ["rien", "pas grand chose", "sieste"],
    "eoghann" : ["manger", "c'est tout", "pour le moment"],
    "ilan" : ["fermer la porte", "et sa trousse"],
    None : ["reveler son identité", "ce sera déjà bien"]
}

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db = DB()
    l = []
    for _,a in db.get(name):
        l.append(a)
    return render_template(
        "user.html",
        name=name,
        todo=l)

@app.route('/users/')
def users():
    db = DB()
    l = []
    noms = set()
    activites = set()
    for name in db.users():
        if name[0] not in noms:
            acts = db.get(name[0])
            for act in acts:
                activites.add(act[1])
            l.append(["Anonyme" if name[0] =="" else name[0], activites])
            activites = set()
            noms.add(name[0])
    return render_template('users.html', data = l)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
